# Ansible FTP Server

playbook to install ftp server with FileBrowser for web based viewing.

## install

You need to have the Jeff Geerling's docker role installed

`ansible-galaxy install geerlingguy.docker`

Then you need to copy the inventory file

`cp example.inventory.yml inventory.yml`

update the `hosts` line with the host you want to install this on.

modify the following vars

- `vsftpd_virt_users`
  - this will set the users and password for vsftpd
- `nginx_hostname`
  - this sets up the nginx config file

## gotchas

this sets up nginx for http only. pretty easy to change to https, but i didn't feel like getting certs from my ca for this.

## defaults

the default login for Filebrowser is `admin:admin`, you can change that in the settings page after launch.

the webui will be available at `http://{{nginx_hostname}}`

## links

Filebrowser: https://github.com/filebrowser/filebrowser

- i will note that the docker run command is wrong for this, it has the location of the db file getting dropped into the root of the container when it should be getting dropped into `/database`. I fixed that in my implementation. There is a PR for this, but it looks like it's not going to get merged.

vsftpd: https://wiki.debian.org/vsftpd

I followed [this](https://julienbourdeau.com/setup-vsftpd-custom-multiple-directories-users-accounts-ubuntu-step-by-step) guide to setup vsftpd and convert it to an ansible playbook.
